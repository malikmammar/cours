
# TELECHARGER GIT


### MAC OS

HOMEBREW:<br>
Install homebrew if you don't already have it, then:<br>
$ brew install git

MACPORTS:<br>
Install MacPorts if you don't already have it, then:<br>
$ sudo port install git

XCODE:<br>
Apple ships a binary package of Git with Xcode.

### WINDOWS

DOWNLOAD:<br>
https://github.com/git-for-windows/git/releases/download/v2.37.3.windows.1/Git-2.37.3-64-bit.exe

### LINUX

https://git-scm.com/download/linux
